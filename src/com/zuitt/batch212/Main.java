package com.zuitt.batch212;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

//  FOR LOOPS
        for(int i=0; i<10; i++){
            System.out.println("Current Count"+i);
        }
        int[] intArray={100,200,300};
        for(int i=0; i<intArray.length; i++){
            System.out.println(intArray[i]);
        }
//  FOR EACH
        String[] nameArray = {"Jon","Paul","George","Ringo"};
        for(String name: nameArray){
            System.out.println(name);
        }
        for(int sample:intArray){
            System.out.println(sample);
        }
//  Nested LOOPS
        String[][] classroom = new String[3][3];
        classroom[0][0] = "Dahyun";
        classroom[0][1] = "Mina";
        classroom[0][2] = "Naeyon";

        classroom[1][0] = "Luffy";
        classroom[1][1] = "Nami";
        classroom[1][2] = "Chopper";

        classroom[2][0] = "Loid";
        classroom[2][1] = "Anya";
        classroom[2][2] = "Yor";
        for(int row = 0; row <3; row++){
            for(int col=0;col<3; col++){
                System.out.println(classroom[row][col]);
            }
        }
//  WHILE LOOP
        int x=0;
        int y=10;
        while(x<10){
            System.out.println("loop number"+x);
            x++;
        }
//  DO_WHILE_LOOP
        do{
            System.out.println("countdown: "+y);
            y--;

        } while (y>0);
//  TRY-CATCH-FINALY STATEMENT
        Scanner input=new Scanner(System.in);
        int num1=0;
        try{
            System.out.println("Enter A Number from 1 to 10");
            num1=input.nextInt();
        }
        catch(InputMismatchException e){
            System.out.println("Invalid is not a Number");
        }
        catch(Exception e){
            System.out.println("Invalid Input");
        }
        finally {
            if(num1 !=0){
                System.out.println("the Number you entered:"+num1);
            }
        }











    }
}
