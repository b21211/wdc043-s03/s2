package com.zuitt.batch212;

import java.util.Scanner;

public class WDC043_S3_A1 {
    public static void main(String[] args) {

        int n, i, f = 1;

        System.out.println("Input an Integer whose factorial will be computed");
        Scanner in = new Scanner(System.in);

        n = in.nextInt();

        if (n < 0)
            System.out.println("Invalid Number.");
        else
        {
            for (i = 1; i <= n; i++)
                f = f*i;

            System.out.println("The Factorial of "+n+" is = "+f);
        }


    }
}
